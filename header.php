<?php
/**
 * The Header for Customizr.
 *
 * Displays all of the <head> section and everything up till <div id="main-wrapper">
 *
 * @package Customizr
 * @since Customizr 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.js"></script>
	<!--<script type="text/javascript">
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 1) {
				$('.google-translate').hide();
			} else {
				$('.google-translate').show();
			}
		});
	</script> -->
<!--<![endif]-->
	<?php
		//the '__before_body' hook is used by TC_header_main::$instance->tc_head_display()
		do_action( '__before_body' );
	?>

	<body <?php body_class(); ?> <?php echo apply_filters('tc_body_attributes' , 'itemscope itemtype="http://schema.org/WebPage"') ?>>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=670319223041198";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
    <?php do_action( '__before_page_wrapper' ); ?>

    <div id="tc-page-wrap" class="<?php echo implode( " ", apply_filters('tc_page_wrap_class', array() ) ) ?>">

  		<?php do_action( '__before_header' ); ?>


			<header class="<?php echo implode( " ", apply_filters('tc_header_classes', array('tc-header' ,'clearfix', 'row-fluid') ) ) ?>" role="banner">
			<div class="hearder-new">
				<div class="google-translate">
					<?php echo do_shortcode('[prisna-google-website-translator]'); ?>
				</div>
				<div class="header-menu-new">
					<?php
					// The '__header' hook is used with the following callback functions (ordered by priorities) :
					//TC_header_main::$instance->tc_logo_title_display(), TC_header_main::$instance->tc_tagline_display(), TC_header_main::$instance->tc_navbar_display()
					do_action( '__header' );
					?>
				</div>
			</div>
			</header>


  		<?php
  		 	//This hook is used for the slider : TC_slider::$instance->tc_slider_display()
  			do_action ( '__after_header' )
  		?>